/**
 * MATH
 */

// 1. Pagliacci charges $16.99 for a 13” pizza and $19.99 for a 17” pizza.
// What is the area for each of these pizzas?
// (radius would be the listed size - i.e. 13" - divided by 2)

let pizzaSizeThirteen = 16.99;
let pizzaSizeSeventeen = 19.99;

let areaPizzaSizeThirteen = Math.PI * (pizzaSizeThirteen / 2) ** 2;
console.log(`Area of 13 inch pizza: ${areaPizzaSizeThirteen}`);

let areaPizzaSizeSeventeen = Math.PI * (pizzaSizeSeventeen / 2) ** 2;
console.log(`Area of 17 inch pizza: ${areaPizzaSizeSeventeen}`);

// 2. What is the cost per square inch of each pizza?

let costOfPizzaSizeThirteen = 16.99 / areaPizzaSizeThirteen;
console.log(`Cost per square inch of 13 inch pizza: ${costOfPizzaSizeThirteen}`);

let costOfPizzaSizeSeventeen = 19.99 / areaPizzaSizeThirteen;
console.log(`Cost per square inch of 17 inch pizza: ${costOfPizzaSizeSeventeen}`);

// 3. Using the Math object, put together a code snippet
// that allows you to draw a random card with a value
// between 1 and 13 (assume ace is 1, jack is 11…)

function getRandomCard (min, max) {
  min = Math.ceil(min);
  math = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1) + min);
}

let card = getRandomCard(1, 13);
console.log(`Card: ${card}`);


// 4. Draw 3 cards and use Math to determine the highest
// card

let card1 = getRandomCard(1, 13);
let card2 = getRandomCard(1, 13);
let card3 = getRandomCard(1, 13);
console.log(`Card 1: ${card1}, Card 2: ${card2}, Card 3: ${card3}`);

const cardArray = [card1, card2, card3];
console.log('Highest card: ' + Math.max(...cardArray));

/**
 * ADDRESS LINE
 */

// 1. Create variables for firstName, lastName,
// streetAddress, city, state, and zipCode. Use
// this information to create a formatted address block
// that could be printed onto an envelope.

let firstName = 'Surry';
let lastName = 'Mowery';
let streetAddress = '123 Fake St';
let city = 'Seattle';
let state = 'WA';
let zipCode = '98101';

let fullAddress = 
`${firstName} ${lastName}
${streetAddress}
${city}, ${state}  ${zipCode}`;

console.log(fullAddress);


// 2. You are given a string in this format:
// firstName lastName(assume no spaces in either)
// streetAddress
// city, state zip(could be spaces in city and state)
// 
// Write code that is able to extract the first name from this string into a variable.
// Hint: use indexOf, slice, and / or substring

let fullAddress2 = 
`Surry Mowery 
321 Maple St
Seattle, WA 98102`;

console.log(fullAddress2);

let firstNameIndex = fullAddress2.indexOf('Surry');
console.log(`Index: ${firstNameIndex}`);

let firstNameSplit = fullAddress2.split(' ')[0];
console.log(`First name: ${firstNameSplit}`);


/**
 * FIND THE MIDDLE DATE
 */
// On your own find the middle date (and time) between the following two dates:
// 1/1/2020 00:00:00 and 4/1/2020 00:00:00
//
// Look online for documentation on Date objects.

// Starting hint:
const startDate = new Date('January 1, 2020 00:00:00');
const endDate = new Date('April 1, 2020 00:00:00');

let middleDate = new Date((startDate.getTime() + endDate.getTime()) / 2);
console.log(`Middle date: ${middleDate}`);










